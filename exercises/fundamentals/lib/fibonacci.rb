# frozen_string_literal: true

# BEGIN
def fib(number)
  return nil if number.negative?
  return number if number <= 1

  fib(number - 1) + fib(number - 2)
end

def fibonacci(number)
  fib(number - 1)
end
# END
