# frozen_string_literal: true

# BEGIN
def get_number(number)
  return 'FizzBuzz' if (number % 3).zero? && (number % 5).zero?
  return 'Fizz' if (number % 3).zero?
  return 'Buzz' if (number % 5).zero?

  number
end

def fizz_buzz(start, stop)
  return '' if start > stop
  return start.to_s if start == stop

  array = []

  (start..stop).each do |i|
    array << get_number(i)
  end

  array.join(' ')
end
# END
