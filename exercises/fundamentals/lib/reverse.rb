# frozen_string_literal: true

# BEGIN
def reverse(string)
  results = String.new
  chars = string.each_char.to_a
  chars.size.times { results << chars.pop }
  results
end
# END
