# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  value = '$#%!'

  text.split.map { |word| stop_words.include?(word) ? value : word }.join(' ')
  # END
end
