# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  return '' if params.empty?

  params.sort.map { |key, value| "#{key}=#{value}" }.join('&')
end
# END
