# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  return nil if version1.empty? || version2.empty?

  version1.split('.').map(&:to_i) <=> version2.split('.').map(&:to_i)
end
# END
